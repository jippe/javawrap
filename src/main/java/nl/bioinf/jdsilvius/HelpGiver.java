
package nl.bioinf.jdsilvius;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.IOException;

/**
 * deze classe maakt options van de cli en stuurt het door naar de WekaRunner
 * om de files die hier als argument zijn meegegeven te classificeren.
 *
 * @author Jippe
 */
public class HelpGiver{
    private static final String HELP = "help";
    private static final String FILENAME = "filename name";
    private static final String INSTANCE = "intances";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;

    /**
     * constructs met the command line array.
     *
     * @param args the CL array
     */
    public HelpGiver(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }


    private void buildOptions() {
        /**
         * maakt options object.
         */
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Do you have the right number of instances or is it scaled data?");
        Option nameOption = new Option("f", FILENAME, true, "Can be .csv or .arff file, must have an extension. gebruik final_testing.arff to use the file from git.");
        Option instanceOption = new Option("i", INSTANCE, true, "Must have 20 items, first is id number, second til nineteenth must be numeric, last is de filename. Between the items must be white spaces.");
        options.addOption(helpOption);
        options.addOption(nameOption);
        options.addOption(instanceOption);
    }


    private void processCommandLine() {
        /**
         * processes the command line arguments.
         * kijkt of de input van de commandline een instance of file is en of het een csv of arff is.
         * manipuleerd de filename zo dat hij ni de wekarunner kan of in de instancemaker/arff file als het een csv file is.
         */
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

            if (this.commandLine.hasOption(FILENAME)){
                String filename = this.commandLine.getOptionValue(FILENAME, "final_tester.arff");

                if (filename.endsWith(".arff")){
                    WekaRunner wekaRunner = new WekaRunner(filename);
                    wekaRunner.start();
                    System.out.println(filename); }

                else if(filename.endsWith(".csv")){
                    String filenaming = filename.replace(".csv","");
                    new TransformIntoARFF(filename);
                    WekaRunner wekaRunner = new WekaRunner(String.format("%s.arff",filenaming));
                    wekaRunner.start();}

                else{System.out.println("File must contain an extension.");}
            }

            else if (this.commandLine.hasOption(INSTANCE)){
                /**
                 * veranderd de arguments van de commandline in een list en kijkt of die 20 items bevat.
                 * maakt van de instance een csv file.
                 * maakt van de csv file een arff
                 */

                String instanceget = this.commandLine.getOptionValue(INSTANCE);
                String[] instanceargs = instanceget.split(" ");

                String filenameofincstance = instanceargs[19];

                if (instanceargs.length == 20) {
                    InstanceMaker instanceMaker = new InstanceMaker(instanceget);
                    instanceMaker.getCorrectInstance();
                    new TransformIntoARFF(filenameofincstance);
                    System.out.println(filenameofincstance);
                    filenameofincstance = filenameofincstance.replace(".csv","");
                    WekaRunner wekaRunner = new WekaRunner(String.format("%s.arff",filenameofincstance));
                    wekaRunner.start();
                }
                else if(instanceargs.length < 20){System.out.println("missing attribute values or an filename.");}
                else if(instanceargs.length > 20){System.out.println("Too many attribute values or filenames");} }



        } catch (ParseException | IOException ex) {
            System.out.println("you need to use a file for -f or 20 args for -a");
        }
    }

    public void printHelp() {
        /**
         * Voor het printen van de help opties.
         */
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("MyHelpOptions", options);
    }

    public boolean helpRequested() {
        /**
         *roept de help functie aan.
         */
        return this.commandLine.hasOption(HELP);
    }

}


