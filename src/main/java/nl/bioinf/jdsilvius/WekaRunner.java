package nl.bioinf.jdsilvius;

import weka.classifiers.meta.Vote;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

/**
 * classificeerd de instances uit de file die mee worden gegeven.
 * @author Jippe
*/

public class WekaRunner {
    /**
     * @param modelfile is als bestand meegegeven.
     * @param file is meegegeven vanuit HelpGiver die is verkregen uit de options van de commandline.
     */
    private final String modelFile = "final_Vote.model";
    private final String file;

    public WekaRunner(String file) {
        this.file = file;
        /**
        als er geen argumenten zijn meegegeven dan moet je zelf de file name nog meegeven.
        anders is de laatste van de argumenten de filenaam.
         */
        }

    private Instances getArffAata(String datafile){
        /**
         return de instances van de arff nadat hij geopent is.
         */

        ConverterUtils.DataSource source;

        try {
            source = new ConverterUtils.DataSource(datafile);
            Instances data = source.getDataSet();
            return data;

        } catch (Exception e) {
            e.printStackTrace();

        }
        return null;
    }


    public void start() {

        /**
         * @param datafile  training data
         * @param unkownArffFile
         * classificeert de instances zonder klasse.
        */
        String unknownArffFile = this.file;

        try {
            Vote fromFile = loadClassifier();

            Instances unknownInstancesFromArff = getArffAata(unknownArffFile);

            if (unknownInstancesFromArff.classIndex() == -1)
                unknownInstancesFromArff.setClassIndex(unknownInstancesFromArff.numAttributes() - 1);

            Instances newData = new Instances(unknownInstancesFromArff);
            // 1. nominal attribute
            Add filter = new Add();
            filter.setAttributeIndex("last");
            filter.setNominalLabels("o, h, s, d");
            filter.setAttributeName("class");
            filter.setInputFormat(newData);
            newData = Filter.useFilter(newData, filter);

            newData.setClassIndex(newData.numAttributes() - 1);
            System.out.println("newData # attributes = " + newData.numAttributes());
            System.out.println("newData class index= " + newData.classIndex());
            System.out.println("unknownInstancesFromCsv = " + newData);

            classifyNewInstance(fromFile, newData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void classifyNewInstance(Vote meta, Instances unknownInstances) throws Exception {
        /**
         * labeled de instances en maakt een copy.
         */
        Instances labeled = new Instances(unknownInstances);
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = meta.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    private Vote loadClassifier() throws Exception {
        /**
         * laad en deserialize de model
         */
        return (Vote) weka.core.SerializationHelper.read(modelFile);
    }
}
