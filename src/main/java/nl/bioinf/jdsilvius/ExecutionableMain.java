package nl.bioinf.jdsilvius;

import java.util.Arrays;

/**
 * Roept de HelpGiver aan.
 * @author Jippe
 */
public class ExecutionableMain {

        /**
         * Voert ExecutionableMain uit omdat deze main function workt gebruikt vanuit build.gradle.
         * print de help als je niks opgeeft.
         */
        public static void main(final String[] args) {
            try {

                HelpGiver op = new HelpGiver(args);
                if (op.helpRequested()) {
                    op.printHelp();
                    return;
                }

            } catch (IllegalStateException|NullPointerException ex) {
                System.err.println("Something went wrong while processing your command line \""
                        + Arrays.toString(args) + "\"");
                System.err.println("Parsing failed.  Reason: " + ex.getMessage());
                HelpGiver op = new HelpGiver(new String[]{});
                op.printHelp();
            }
        }
    }

