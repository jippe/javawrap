package nl.bioinf.jdsilvius;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class filereader {

    public filereader(String argsstring) {
        /*
        lees de csv file in wanneer je die meegeeft in de commandline. en maakt er per regel een lijst van.
        */
        String csvFile = String.format("/homes/jdsilvius/jaar_3/jaar_3_Thema_9/thema9/%s", argsstring);
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ",";

        try {

            br = new BufferedReader(new FileReader(csvFile));
            while ((line = br.readLine()) != null) {

                // use comma as separator
                String[] instance = line.split(cvsSplitBy);
                for (String i:instance){
                 System.out.println(i);}
                System.out.println(instance);
                //System.out.println(Arrays.toString(instance));
                //new weka_runner(csvFile);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
