package nl.bioinf.jdsilvius;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;
import java.io.File;
import java.io.IOException;

/**
 * Maakt een arff file van de contents van de csv.
 * @author Jippe
 */

public class TransformIntoARFF {


    public TransformIntoARFF(String nameofnewfile) throws IOException {
        /**
         * Roept de saveInstanceToArffFile aan.
         */
        saveInstancesToArffFile(nameofnewfile);
    }

    public File saveInstancesToArffFile(String nameofnewfile) throws IOException
            /**
            Laadt de csv file in en maakt er een arff file van.
             als de filnaam eindigt op een .csv haalt hij de .csv eraf.
             */
    {
        String filename = nameofnewfile;
        CSVLoader csvLoader = new CSVLoader();
        csvLoader.setSource(new File(filename));
        Instances instances = csvLoader.getDataSet();
        if (filename.endsWith(".csv")){
            filename = filename.replace(".csv","");

        File outputFile = new File(String.format("%s.arff",filename));
        if (outputFile.exists())
        {
            outputFile.delete();
            outputFile.createNewFile();
        }

        ArffSaver arffSaver = new ArffSaver();
        arffSaver.setInstances(instances);
        arffSaver.setFile(outputFile);
        arffSaver.writeBatch();

        System.out.println(String.format("created arff file: %s",outputFile));

        return arffSaver.retrieveFile();}

        else{
            filename = filename.replace(".csv","");

            File outputFile = new File(String.format("%s.arff",filename));
            if (outputFile.exists())
            {
                outputFile.delete();
                outputFile.createNewFile();
            }

            ArffSaver arffSaver = new ArffSaver();
            arffSaver.setInstances(instances);
            arffSaver.setFile(outputFile);
            arffSaver.writeBatch();

            System.out.println(String.format("created arff file: %s",outputFile));

            return arffSaver.retrieveFile();}
    }
}
