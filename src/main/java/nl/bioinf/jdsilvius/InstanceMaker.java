package nl.bioinf.jdsilvius;


import java.io.*;
import java.util.List;

/**
 * Hier worden de instances naar een file geschreven
 */

public class InstanceMaker{
    private final String instances;

    public InstanceMaker(String instances) {
        this.instances = instances;
    }


    public void getCorrectInstance(){
        /**
         * makes of a instance an csv file.
         * @param instanceargs from options in the command line.
         * @param nameofnewfile is de laatst meegegeven arg.
         */
        String[] instanceargs = this.instances.split(" ");
        String nameofnewfile = instanceargs[19];

        System.out.println(instanceargs[19]);
        System.out.println("You entered: " + instanceargs[19] + ", and now it has become: " + nameofnewfile);

        try (PrintWriter writer = new PrintWriter(new File(nameofnewfile))) {

            StringBuilder sb = new StringBuilder();
            List<String> list = List.of("","b1","b2","b3","b4","b5","b6","b7","b8","b9","pred_minus_obs_H_b1","pred_minus_obs_H_b2","pred_minus_obs_H_b3","pred_minus_obs_H_b4","pred_minus_obs_H_b5","pred_minus_obs_H_b6","pred_minus_obs_H_b7","pred_minus_obs_H_b8","pred_minus_obs_H_b9");

            for (String item:list){

                sb.append("\"");
                sb.append(item);
                sb.append("\"");
                sb.append(",");
            }

            sb.append(",");
            sb.append("\n");

            for (String arg: instanceargs) {
                if (arg == instanceargs[0]){
                    arg = arg.replace(arg, String.format("\"%s\"", instanceargs[0]));
                    sb.append(arg);
                    sb.append(",");
                }
                else if(arg == instanceargs[19]){
                    System.out.println("instance name out of file.");
                }
                else{
                sb.append(arg);
                sb.append(",");
                }
            }
            sb.append(",");

            String hb = sb.toString();
            hb = hb.replace(",,", "");
            System.out.println(hb);
            writer.write(hb);
            System.out.println("done!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}